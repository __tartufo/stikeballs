package io.tartufo.strikeball.Commons;

import java.io.Serializable;

public class Message implements Serializable {
    String body;
    String playerName;

    public Message(String body, String playerName) {
        this.body = body;
        this.playerName = playerName;
    }

    public String getBody() {
        return body;
    }

    public String getPlayerName() {
        return playerName;
    }

}
