package io.tartufo.strikeball.Commons;

import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Random;

/**
 * Rappresenta una combinazione di sb.
 * Implementa i metodi per il suo controllo.
 */
public class Combination implements Serializable {
	int[] digits;
	String[] colors;

	boolean empty;
	boolean correct;
	Instant timestamp;

	private Combination() {
		Random random = new Random();

		digits = new int[4];
		colors = new String[4];
		for (int i = 0; i < 4; i++) {
			digits[i] = random.nextInt(10);
			colors[i] = "grey";
		}
		empty = false;
		correct = false;
		timestamp = Instant.now();
	}


	public static Combination getRandomCombination() {
		return new Combination();
	}

	private Combination getCorrectCombination(Combination c) {
		c.correct = true;
		for (int i = 0; i < 4; i++) {
			colors[i] = "green";
		}
		return c;
	}

	public Combination checkCombination(Combination c) {
		if (empty) {
            c.correct = false;
            for (int i = 0; i < 4; i++) {
                colors[i] = "grey";
            }
            return c;
        } else {
			return Arrays.equals(digits, c.digits) ?
					getRandomCombination() : checkValues(c);
		}
	}

	private Combination checkValues(Combination combination) {
        return getRandomCombination(); //TODO riprendere da vecchio progetto controllo combinazione
    }

	public boolean isCorrect() {
		return correct;
	}

	public Instant getTimestamp() {
		return timestamp;
	}

    /**
     * Should be called right after receiving a Combination from client
     *
     * @param instant The instant the server receive the compination
     */
	public void setTimestamp(Instant instant) {
		this.timestamp = instant;
	}

	public boolean isInTime(Combination c) {
		return Duration.between(c.getTimestamp(), this.timestamp).toSeconds() <= 15;
	}
}
