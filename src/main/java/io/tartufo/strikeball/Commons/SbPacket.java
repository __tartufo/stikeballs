package io.tartufo.strikeball.Commons;


import java.io.Serializable;

/**
 * "StrikeBall Packet"
 */
public class SbPacket implements Serializable {
    String type;

    Message message;
    Combination combination;

    private SbPacket() {
    }

    public SbPacket(Message message) {
        type = "message";
        this.message = message;
    }

    SbPacket(Combination combination) {
        type = "attempt";
        this.combination = combination;
    }

    public static SbPacket request() {
        SbPacket sbPacket = new SbPacket();
        sbPacket.type = "request";
        return sbPacket;
    }

    public Combination getCombination() {
        return combination;
    }

    public Message getMessage() {
        return message;
    }

    public String getType() {
        return type;
    }

    public boolean isMessage() {
        return this.type.equalsIgnoreCase("message");
    }

    public boolean isCombination() {
        return this.type.equalsIgnoreCase("attempt");
    }

    public boolean isFromServer() {
        if (this.isMessage()) {
            return this.getMessage().getPlayerName().equalsIgnoreCase("SERVER");
        }
        return false;
    }
}
