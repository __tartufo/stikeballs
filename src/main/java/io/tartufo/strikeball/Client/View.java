package io.tartufo.strikeball.Client;

import io.tartufo.strikeball.Commons.Message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class View {
    String name;

    void writeChat(Message message) {
        System.out.printf("[CHAT] (%s): %s\n", message.getPlayerName(), message.getBody());
    }

    void writeGame(String message) {
        System.out.println(message);
    }

    void play() throws IOException {
        //TODO lanterna code
        System.out.println("Please insert you name");
        name = new BufferedReader(new InputStreamReader(System.in)).readLine();
    }

    public String getName() {
        return name;
    }
}
