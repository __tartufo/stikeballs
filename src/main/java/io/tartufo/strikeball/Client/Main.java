package io.tartufo.strikeball.Client;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        View console = new View();
        ConnectionHandler connection = new ConnectionHandler(console);
        try {
            console.play();
        } catch (IOException e) {
            e.printStackTrace();
        }
        connection.start();
    }
}
