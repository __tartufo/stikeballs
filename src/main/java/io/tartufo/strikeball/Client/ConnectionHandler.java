package io.tartufo.strikeball.Client;


import io.tartufo.strikeball.Commons.Message;
import io.tartufo.strikeball.Commons.SbPacket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ConnectionHandler extends Thread {
    Socket socket;
    ObjectOutputStream outputStream;
    ObjectInputStream inputStream;
    View view;

    ConnectionHandler(View view) {
        try {
            socket = new Socket("localhost", 3855);
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            inputStream = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.view = view;
    }

    @Override
    public void run() {
        //TODO Listen client
        System.out.printf("[ConnectionHandler/run/INFO] [%s] Listening\n", view.getName());
        try {
            outputStream.writeObject(new SbPacket(new Message(view.getName(), "INIT")));

            while (true) {
                SbPacket p = (SbPacket) inputStream.readObject();
                if (p.isCombination()) {
                    view.writeGame("Hai fatto... " + p.getCombination().isCorrect());
                } else {
                    view.writeChat(p.getMessage());
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
