package io.tartufo.strikeball.Server;

import io.tartufo.strikeball.Commons.Combination;
import io.tartufo.strikeball.Commons.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;

/**
 * Questa classe implementa la logica di gioco.
 * Si occupa di controllare la combinazione, gestire i tentativi, decretare un vincitore...
 */
public class Game extends SubmissionPublisher<Message> implements Flow.Subscriber<Message> {
    static final int MAX_ATTEMPTS = 7;
    static final int MAX_TIMEOUT = 15;
    static final int MAX_PLAYERS = 10;
    static final String SERVER_GAME = "SERVER";

    Combination correctCombination;
    int playersCount;
    Flow.Subscription subscription;
    List<String> ids;
    ExecutorService pool = Executors.newFixedThreadPool(Game.MAX_PLAYERS);

    Game() {
        super();
        this.correctCombination = Combination.getRandomCombination();
        this.playersCount = 0;
        ids = new ArrayList<>();
    }

    void addPlayer(ClientHandler p) {
        this.subscribe(p);
        this.playersCount++;
        pool.execute(p);
        System.out.println("[Game/addPlayer/INFO] Adding a player");
    }

    void addId(String id) {
        this.ids.add(id);
    }


    int getPlayersNumber() {
        return this.playersCount;
    }

    // Subscriber
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        subscription.request(1);
        System.out.println("[Game/onSubscribe/INFO] Subscribed to a client.");
    }

    @Override
    public void onNext(Message message) {
        System.out.printf("[Game/onNext/INFO] Got message from %s. Forwarding to other players\n", message.getPlayerName());
        this.submit(message);
    }

    /**
     * Riceve i dati dal giocatore, li processa e risponde con i risultati.
     *
     * @param inAttempt the incoming attempt we need to check
     */
    public Combination checkCombination(Combination inAttempt, String playerName) {
        System.out.printf("[Game/checkCombination/INFO] Got an attempt from player %s\n", playerName);
        Combination result = correctCombination.checkCombination(inAttempt);
        if (result.isCorrect()) {
            this.submit(new Message("We got a WINNER: " + playerName, "SERVER"));
        }
        return result;
    }

    public void onError(Throwable throwable) {
        System.out.printf("[Game/onError/ERROR] Got error: %s", throwable.getMessage());
    }

    public void onComplete() {
        System.out.println("[Game/onComplete/INFO] Completed requests queue.");
    }

}

