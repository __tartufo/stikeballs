package io.tartufo.strikeball.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ConnectionLister {
    Game game;
    ServerSocket sSocket;

    ConnectionLister(Game game) {
        this.game = game;

        try {
            sSocket = new ServerSocket(3855);
            System.out.println("Listening thread started");
            while (game.getPlayersNumber() < Game.MAX_PLAYERS) {
                Socket s = sSocket.accept();
                System.out.println("Accepted connection");
                game.addPlayer(new ClientHandler(s, game));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
