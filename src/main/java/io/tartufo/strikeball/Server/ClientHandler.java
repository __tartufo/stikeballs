package io.tartufo.strikeball.Server;

import io.tartufo.strikeball.Commons.Combination;
import io.tartufo.strikeball.Commons.Message;
import io.tartufo.strikeball.Commons.SbPacket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;

/**
 * Gestisce la connessione con il client
 */
public class ClientHandler extends SubmissionPublisher<Message> implements Flow.Subscriber<Message>, Runnable {
    String name;
    Game game;
    Flow.Subscription subscription;
    ObjectInputStream inputStream;
    ObjectOutputStream outputStream;

    int madeAttempts;

    ClientHandler(Socket playerSocket, Game game) {
        super();
        this.game = game;
        madeAttempts = 0;
        try {
            this.inputStream = new ObjectInputStream(playerSocket.getInputStream());
            this.outputStream = new ObjectOutputStream(playerSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Incoming messages from other players.
     *
     * Part of the Subscriber interface.
     *
     * @param message the received message.
     */
    @Override
    public void onNext(Message message) {
        System.out.printf("[ClientHandler/onNext/INFO] [%s] Got Message from %s: \"%s\". Sending.\n", name, message.getPlayerName(), message.getBody());
        send(new SbPacket(message));
        subscription.request(1);
    }


    /**
     * Called when the Game subscribe to this client for messages.
     */
    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        System.out.printf("[ClientHandler/INFO] [%s] Subscribed to game logic server.\n", name);
        this.subscription = subscription;
        subscription.request(1);
    }

    @Override
    public void onError(Throwable error) {
        System.out.printf("[ClientHandler/ERROR] [%s] Got error: %s\n", name, error.getMessage());
    }

    @Override
    public void onComplete() {
        System.out.printf("[ClientHandler/INFO] [%s] Game Completed\n", name);
    }

    // Implementing Runnable interface
    @Override
    public void run() {
        this.initialize();
        this.listen();
    }

    private void initialize() {
        send(new SbPacket(new Message("NAME", "INIT")));
        SbPacket p = listenPacket();
        if (p != null) {
            Message name = p.getMessage();
            if (name.getPlayerName().equalsIgnoreCase("INIT")) {
                System.out.println("[ClientHandler/listen/INFO] got name from player: " + name.getBody());
                this.name = name.getBody();
            }
        }

        this.subscribe(game);
    }

    private void listen() {
        while (madeAttempts <= Game.MAX_ATTEMPTS) {
            SbPacket sbp = listenPacket();
            assert sbp != null;
            if (sbp.isMessage()) {
                this.submit(sbp.getMessage());
            } else if (sbp.isCombination()) {
                Combination result = game.checkCombination(sbp.getCombination(), this.name);

                madeAttempts++;
            }
        }
        send(new SbPacket(new Message("Seems like you ended yout attemps...", Game.SERVER_GAME)));
    }

    private void send(SbPacket message) {
        if (message != null) {
            try {
                outputStream.writeObject(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private SbPacket listenPacket() {
        try {
            return (SbPacket) inputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}
